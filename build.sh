!/bin/sh -x
set -e
echo "Build started on $(date)"
echo "Build Pointzi Pod ==================="
pod update

#pod repo add pzswiftframework https://gitlab.com/pointzi/sdks/pointzi-sdk-ios-swift-podframework.git
pod repo
xcodebuild clean build  -workspace PointziPod.xcworkspace -scheme PointziPod -sdk iphonesimulator
#pod repo remove pzswiftframework
#pod lib lint PZSwiftFramework.podspec

PRERELEASE_SUFFIX=""
PRERELEASE_SUFFIX_CLASSIFIER=""
PRERELEASE_SUFFIX_REV=""
BASEREV=$(git -C "$1" describe --tags --abbrev=0)
if [ -z "$(git -C "$1" tag -l --points-at HEAD | grep -v '^_')" ]; then
    PRERELEASE_SUFFIX_CLASSIFIER="-beta"
    PRERELEASE_SUFFIX_REV="$(git -C "$1" log -1 --format="%h")"
    PRERELEASE_SUFFIX="$PRERELEASE_SUFFIX_CLASSIFIER+$PRERELEASE_SUFFIX_REV"
    BASEREV=$(git -C "$1" describe --tags --abbrev=0 | python -c 'import semver, sys;print(semver.bump_patch(sys.stdin.read()))')
fi

echo ${PRERELEASE_SUFFIX}

if [ "$PRERELEASE_SUFFIX" == "" ]; then
   echo "Publishing pod"
   pod trunk push PZSwiftFramework.podspec --allow-warnings
fi


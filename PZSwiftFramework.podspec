#
# Be sure to run `pod lib lint Umbrella.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
#

Pod::Spec.new do |s|
s.name              = 'PZSwiftFramework'
s.version           = '0.0.6'
s.summary           = 'Description of PZ Framework.'

s.description      = <<-DESC
A bigger description of PZ Framework.
DESC

s.homepage          = 'https://pointzi.com'

s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    LICENSE
}

s.authors           = { 'Author one' => 'ganeshfaterpekar@gmail.com'}
s.source            = { :http => 'https://pkg.streethawk.com/artifactory/pointzi-ios/streethawk/pointzi-sdk-ios-swift/develop/-beta+2a3ab6a/Pointzi.zip'}
s.public_header_files = "Pointzi/PointziFramework.framework/Headers/*.h"
s.source_files = "Pointzi/PointziFramework.framework/Headers/*.h"

s.swift_version = "5.0"
s.ios.deployment_target = '10.0'
s.ios.vendored_frameworks = 'Pointzi/PointziFramework.framework'

# Add all the dependencies
s.dependency 'Alamofire'

end
